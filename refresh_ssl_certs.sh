#!/bin/bash

# Borrowed from Gino, thanks Gino! :) 

DOMAIN="0to1.nl"
export DOMAIN

# script to move the latest ssl certs for Haproxy
# create on Sunday 5th of May by Gino Eising

function move_certs {
EXPIRATION=$(openssl x509 -checkend $(( 86400 * 30 )) -noout -in /etc/haproxy/certs/wildcard.$i.pem)
if [[ $EXPIRATION == "Certificate will not expire" ]]; then

        echo "Cert $i is going to be replaced"
        cp /home/letsencrypt/wildcard.$i.pem /etc/haproxy/certs/wildcard.$i.pem
else

        echo "Cert $i is valid so not doing anything today"
fi
}

function for_domains {

for i in "${DOMAIN}"
do
        move_certs
done
}

function reload_haproxy {

DOMAIN_WILD=wildcard.${DOMAIN}.pem

if [[ /home/letsencrypt/$DOMAIN_WILD -ot /etc/haproxy/certs/$DOMAIN_WILD ]]; then
        echo "Going reload HAproxy since certificates are newer"
        /etc/init.d/haproxy reload
else
        echo "Certificates aren't newer so not doing anything today"
fi

}

# main
for_domains
reload_haproxy
